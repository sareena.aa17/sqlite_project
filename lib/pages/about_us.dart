import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  const AboutUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Us'),
      ),
      body: Container(
        padding: EdgeInsets.all(50),
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: const [
              CircleAvatar(
                  radius: 100,
                  backgroundImage: AssetImage('assets/images/na.jpg',)
              ),
              SizedBox(height: 15,),
              Text('6450110004', style: TextStyle(fontSize: 20),),
              Text('นางสาวซารีณา ชูจันทร์', style: TextStyle(fontSize: 20),),
              SizedBox(height: 50,),
              CircleAvatar(
                radius: 100,
                backgroundImage: AssetImage('assets/images/min.jpg'),
              ),
              SizedBox(height: 15,),
              Text('6450110017', style: TextStyle(fontSize: 20),),
              Text('นางสาวกิตติมา ช้างขาว', style: TextStyle(fontSize: 20),),
              SizedBox(height: 50,),
            ],
          ),
        ),
      ),
    );
  }
}
