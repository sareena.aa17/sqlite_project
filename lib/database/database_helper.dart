import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqlite_project/model/profile_model.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'db_foods.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE foodDB (
          id INTEGER PRIMARY KEY,
          foodname TEXT,
          price TEXT,
          image TEXT
      )
      ''');
  }

  Future<List<FoodModel>> getFoods() async {
    Database db = await instance.database;
    var foods = await db.query('foodDB', orderBy: 'foodname');
    List<FoodModel> groceryList = foods.isNotEmpty
        ? foods.map((c) => FoodModel.fromMap(c)).toList()
        : [];
    return groceryList;
  }

  Future<List<FoodModel>> getFood(int id) async{
    Database db = await instance.database;
    var food = await db.query('foodDB', where: 'id = ?', whereArgs: [id]);
    List<FoodModel> foodList = food.isNotEmpty
        ? food.map((c) => FoodModel.fromMap(c)).toList()
        : [];
    return foodList;
  }

  Future<int> add(FoodModel food) async {
    Database db = await instance.database;
    return await db.insert('FoodDB', food.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('FoodDB', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(FoodModel food) async {
    Database db = await instance.database;
    return await db.update('FoodDB', food.toMap(),
        where: "id = ?", whereArgs: [food.id]);
  }
}